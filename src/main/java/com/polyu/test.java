package com.polyu;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.compress.utils.IOUtils;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by januslin on 23/4/2017.
 */
public class test {
    public static void main(String[] args) throws Exception{
        File fi = new File("MyMultiLayerNetwork.zip");

        try {
            imageNet.imageNet = ModelSerializer.restoreMultiLayerNetwork(fi, true);

            imageNet.imageNet.init();
            //System.out.println("Saved and loaded parameters:      " + (imageNet.imageNet.params()));
            //System.out.println("Saved and loaded configurations :  " + (imageNet.imageNet.getLayerWiseConfigurations()));
            System.out.println("Model loaded.");

        } catch (Exception e) {
            e.printStackTrace();
        }

        //InputStream is = file..openStream();
        Path path = Paths.get("8.jpg");
        byte[] bytes = Files.readAllBytes(path);

        //byte[] bytes = IOUtils.toByteArray(is);
        String imageString = encodeImage(bytes);

        System.out.println("imageString=" + imageString);

        NativeImageLoader loader = new NativeImageLoader(100, 100, 3);
        byte[] data = Base64.decodeBase64(imageString);
        InputStream stream = new ByteArrayInputStream(data);
        INDArray image = loader.asMatrix(stream);

        System.out.println("Length = "+String.valueOf(image.length()));

        DataNormalization scaler = new ImagePreProcessingScaler(0,1);
        scaler.transform(image);

        //INDArray output = imageNet.imageNet.output(image);
        //INDArray labels = new
        //imageNet.imageNet.setLabels();
        int[] predict = imageNet.imageNet.predict(image);
        //INDArray labels = imageNet.imageNet.getLabels();
        /*for (int num :
                predict) {
            System.out.println(num);
        }*/
        List<String> labels = new ArrayList<>();
        labels.add("2");
        labels.add("8");
        labels.add("9");
        labels.add("10");
        labels.add("21");
        labels.add("26");

        System.out.println(labels.get(predict[0] - 1));
    }

    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }
}
