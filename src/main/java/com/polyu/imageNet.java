package com.polyu;

import org.apache.commons.codec.binary.Base64;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import spark.Request;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by januslin on 23/4/2017.
 */
public class imageNet {
    static MultiLayerNetwork imageNet;

    public static String predict(String filename) {
        try {
            NativeImageLoader loader = new NativeImageLoader(100, 100, 3);
            //byte[] data = Base64.decodeBase64();
            //InputStream stream = new ByteArrayInputStream(data);
            File file = new File(filename);
            INDArray image = loader.asMatrix(file);

            System.out.println("Length = "+String.valueOf(image.length()));

            DataNormalization scaler = new ImagePreProcessingScaler(0,1);
            scaler.transform(image);

            //INDArray output = imageNet.imageNet.output(image);
            //INDArray labels = new
            //imageNet.imageNet.setLabels();
            int[] predict = imageNet.predict(image);
            //INDArray labels = imageNet.imageNet.getLabels();
        /*for (int num :
                predict) {
            System.out.println(num);
        }*/
            List<String> labels = new ArrayList<>();
            labels.add("2");
            labels.add("8");
            labels.add("9");
            labels.add("10");
            labels.add("21");
            labels.add("26");

            file.delete();

            return labels.get(predict[0] - 1);
        } catch (IOException e) {
            return null;
        }
    }
}
