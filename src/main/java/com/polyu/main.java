package com.polyu;

import org.apache.commons.codec.binary.Base64;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static spark.Spark.*;
import static spark.Spark.internalServerError;

/**
 * Created by januslin on 23/4/2017.
 */
public class main {

    public static void main(String[] args) {
        File fi = new File("MyMultiLayerNetwork.zip");

        try {
            imageNet.imageNet = ModelSerializer.restoreMultiLayerNetwork(fi, true);
            imageNet.imageNet.init();

            System.out.println("Saved and loaded parameters:      " + (imageNet.imageNet.params()));
            System.out.println("Saved and loaded configurations :  " + (imageNet.imageNet.getLayerWiseConfigurations()));
            System.out.println("Model loaded.");

        } catch (Exception e) {
            e.printStackTrace();
        }
        port(8080);

        int maxThreads = 8;
        int minThreads = 2;
        int timeOutMillis = 30000000;
        threadPool(maxThreads, minThreads, timeOutMillis);


        post("/predict", (req, res) -> {
            res.type("application/json");
            //if (imageNet.imageNet == null) {
            //    internalServerError("{\"status\":\"505\"}");
            //}

            Date date = new Date();
            DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSS");

            System.out.println("img="+req.queryParams("img"));
            byte[] data = Base64.decodeBase64(req.queryParams("img"));
            String filename =sdf.format(date)+".jpg";
            try (OutputStream stream = new FileOutputStream(filename)) {
                stream.write(data);
            }

            String result = imageNet.predict(filename);

            return "{\"result\":" + result +"}";
        });
        get("/hello", (req, res) -> "Hello World");
        notFound((req, res) -> {
            res.type("application/json");
            return "{\"status\":\"404\"}";
        });
        internalServerError((req, res) -> {
            res.type("application/json");
            return "{\"status\":\"505\"}";
        });
    }
}
